# Pi Switcher

## A small flask application to control the dry switch relays connected by serial

### Notes:
 
 - Application assumes that serial port is on /dev/ttyUSB0
 - simply run with
 > python switcher.py
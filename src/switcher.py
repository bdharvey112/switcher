from flask import Flask
from flask import jsonify
import time
import serial

app = Flask(__name__)
# configure the serial connections (the parameters differs on the device you are connecting to)
ser = serial.Serial(
    port='/dev/ttyUSB0',
    baudrate=9600,
    parity=serial.PARITY_ODD,
    stopbits=serial.STOPBITS_TWO,
    bytesize=serial.SEVENBITS
)


def run_command(_input):
    ser.isOpen()
    # send the character to the device
    # (note that I happend a \r\n carriage return and line feed to the characters - this is requested by my device)
    ser.write(_input)
    out = ''
    # let's wait one second before reading output (let's give device time to answer)
    time.sleep(1)
    while ser.inWaiting() > 0:
        out += ser.read(1)

    if out != '':
        print ">>" + out


@app.route('/on/<int:switch_no>')
def switch_on(switch_no):

    returnDict = {
        'status': {
            'switch': str(switch_no),
            'status': 'on'
        }
    }

    command = 'n' + str(switch_no).encode("ascii") + '\r\n'
    try:
        run_command(command)
        return jsonify(**returnDict) 
    except Exception:
        # Do something
        pass


@app.route('/off/<int:switch_no>')
def switch_off(switch_no):

    returnDict = {
        'status': {
            'switch': str(switch_no),
            'status': 'off'
        }
    }

    command = 'f' + str(switch_no).encode("ascii") + '\r\n'
    try:
        run_command(command)
        return jsonify(**returnDict) 
    except Exception:
        # Do something
        pass

if __name__ == '__main__':
    app.run(host="0.0.0.0")
